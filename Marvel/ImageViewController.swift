//
//  ImageViewController.swift
//  Marvel
//
//  Created by mastermoviles on 23/11/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import UIKit
import Marvelous

class ImageViewController: UIViewController
{
    var comic: RCComicsObject!
    @IBOutlet weak var imagen: UIImageView!
    
    func cargarImagen()
    {
        let colaBackGround = OperationQueue()
        
        colaBackGround.addOperation
        {
            if let thumb = self.comic.thumbnail
            {
                let url = "\(thumb.basePath!).\(thumb.extension!)"
                let urlHttps = url.replacingOccurrences(of: "http", with: "https")
                if let urlFinal = URL(string : urlHttps)
                {
                    do
                    {
                        let datos = try Data(contentsOf : urlFinal)
                        if let img = UIImage(data : datos)
                        {
                            OperationQueue.main.addOperation
                                {
                                    self.imagen.image = img
                            }
                        }
                    }
                    catch
                    {
                        
                    }
                }
            }
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        cargarImagen()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
