//
//  ViewController.swift
//  Marvel
//
//  Created by Otto Colomina Pardo on 5/11/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import UIKit
import Marvelous


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mostrarPersonajes(comienzanPor: "Spider")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mostrarPersonajes(comienzanPor cadena : String) {
        let marvelAPI = RCMarvelAPI()
        //PUEDES CAMBIAR ESTO PARA PONER TUS CLAVES
        marvelAPI.publicKey = "c18805b352e2589cbfed50811d12e98a"
        marvelAPI.privateKey = "854ce0bc32567a6e0ccdd22fadb26de3a0dc6211"
        let filtro = RCCharacterFilter()
        filtro.nameStartsWith = cadena
        marvelAPI.characters(by: filtro) {
            resultados, info, error in
            if let personajes = resultados as! [RCCharacterObject]? {
                for personaje in personajes {
                    print(personaje.name)
                }
                print("Hay \(personajes.count) personajes")
            }
        }
    }


}

