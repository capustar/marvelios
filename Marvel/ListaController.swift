//
//  ListaController.swift
//  Marvel
//
//  Created by Marcelo on 21/11/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import UIKit
import Marvelous


class ListaController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate
{
    var lista_comics = [RCComicsObject]()
    
    @IBOutlet weak var tablaComics: UITableView!
    
    func mostrarComics(comienzanPor cadena : String)
    {
        let marvelAPI = RCMarvelAPI()
        marvelAPI.publicKey = "c18805b352e2589cbfed50811d12e98a"
        marvelAPI.privateKey = "854ce0bc32567a6e0ccdd22fadb26de3a0dc6211"
    
        let filtro = RCComicsFilter()
        filtro.titleStartsWith = cadena
        
        self.lista_comics.removeAll()
        
        marvelAPI.comics(by: filtro)
        {
            resultados, info, error in
            if let comics = resultados as! [RCComicsObject]?
            {
                self.lista_comics = comics
                
                for comic in comics
                {
                    print(comic.title)
                }
                print("Hay \(self.lista_comics.count) comics")
            
                OperationQueue.main.addOperation()
                {
                        self.tablaComics.reloadData();
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        print("Buscado: \(searchBar.text!)")
        
        mostrarComics(comienzanPor : searchBar.text!)
        searchBar.resignFirstResponder()
        
        searchBar.text = ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.lista_comics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let celda = UITableViewCell(style: .default, reuseIdentifier: "comicCell")
        //la rellenamos de contenido
        celda.textLabel?.text = self.lista_comics[indexPath.row].title
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performSegue(withIdentifier: "detalles", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var comic: RCComicsObject
        let vc2 = segue.destination as! DetalleViewController
        let indexPath = self.tablaComics.indexPathForSelectedRow
        
        comic = self.lista_comics[(indexPath?.row)!]
        print(comic.title)
        vc2.comic = self.lista_comics[(indexPath?.row)!]
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tablaComics.delegate = self

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
